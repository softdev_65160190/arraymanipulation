public class ArrayManipulation {
    public static void main(String[] args) {

        // An integer array named "numbers" with the values {5, 8, 3, 2, 7}.
        int[] numbers = { 5, 8, 3, 2, 7 };

        // A string array named "names" with the values {"Alice", "Bob", "Charlie",
        // "David"}.
        String[] names = { "Alice", "Bob", "Charlie", "David" };

        // A double array named "values" with a length of 4 (do not initialize the
        // elements yet).
        double[] values = new double[4];

        // 3. Print the elements of the "numbers" array using a for loop.
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println(" ");

        // 4. Print the elements of the "names" array using a for-each loop.
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println(" ");

        // 5.Initialize the elements of the "values" array with any four decimal values
        // of your choice.
        values[0] = 1205;
        values[1] = 8487;
        values[2] = 3247;
        values[3] = 607;
        // 6.Calculate and print the sum of all elements in the "numbers" array.
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        System.out.print(sum + " ");
        System.out.println(" ");
        // 7.Find and print the maximum value in the "values" array.
        double max = 0;
        for (int i = 0; i < values.length; i++){
            if(values[i] > max){
                max = values[i];
            }
        }
        System.out.println(max);
        // 8.Create a new string array named "reversedNames" with the same length as the
        // "names" array.
        // -Fill the "reversedNames" array with the elements of the "names" array in
        // reverse order.
        // -Print the elements of the "reversedNames" array.

        String reversedNames[] = new String[names.length];
        for (int i =0; i < names.length; i++){
            reversedNames[names.length -1 -i] = names[i];
        }
        for (String name : reversedNames) {
            System.out.print(name+" ");
        }
    }
}